import { RecipeBookAdminPage } from './app.po';

describe('recipe-book-admin App', () => {
  let page: RecipeBookAdminPage;

  beforeEach(() => {
    page = new RecipeBookAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
