import { Injectable } from '@angular/core';

import { Breadcrumb } from '../breadcrumb/breadcrumb.model';
import { TranslateService } from './translate/translate.service';
import { LANG_EN_NAME } from './translate/lang-en';
import { LANG_ZH_NAME } from './translate/lang-zh';

@Injectable()
export class BreadcrumbService {

  private baseBreadcrumb: Breadcrumb = {
    title: this.translateService.translate('home', LANG_EN_NAME),
    title_zh: this.translateService.translate('home', LANG_ZH_NAME),
    url: 'home'
  };

  private breadcrumbs: Breadcrumb[] = [ this.baseBreadcrumb ];

  constructor(private translateService: TranslateService) {}


  setBreadcrumb(breadcrumbs: Breadcrumb[]) {
    this.breadcrumbs = [ this.baseBreadcrumb ];
    this.breadcrumbs.push(...breadcrumbs);
  }

  pushBreadcrumb(breadcrumbs: Breadcrumb[]) {
    this.breadcrumbs.push(...breadcrumbs);
  }

  getBreadcrumbs() {
    return this.breadcrumbs.slice();
  }

  disableBreadcrumbs() {
    this.breadcrumbs = [];
  }
}
