import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';
import { ModalService } from './modal/modal.service';
import { TranslateService } from './translate/translate.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService,
              private router: Router,
              private modalService: ModalService,
              private translateService: TranslateService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.isAuthenticated()) {
      this.modalService.show({ content: this.translateService.translate('loginRequired') });
      this.router.navigateByUrl('/login');
    }
    return this.authService.isAuthenticated();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.authService.isAuthenticated()) {
      this.modalService.show({ content: this.translateService.translate('loginRequired') });
      this.router.navigateByUrl('/login');
    }
    return this.authService.isAuthenticated();
  }
}
