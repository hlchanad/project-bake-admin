import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';

import * as HttpStatus from 'http-status-codes';

import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {

  constructor(private http: Http,
              private authService: AuthService,
              private router: Router) {}

  getWithLogin(url: string): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      this.http.get(url, this.generateRequestOptionsWithLogin())
        .subscribe(
          (response: Response) => {
            this.refreshAccessToken(response.json().data.user);
            resolve(response);
          },
          (error) => {
            this.handleExpiredTokenError(error);
            reject(error);
          }
        );
    });
  }

  postWithLogin(url: string, body: any): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      this.http.post(url, body, this.generateRequestOptionsWithLogin())
        .subscribe(
          (response: Response) => {
            this.refreshAccessToken(response.json().data.user);
            resolve(response);
          },
          (error) => {
            this.handleExpiredTokenError(error);
            reject(error);
          }
        );
    });
  }

  putWithLogin(url: string, body: any): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      this.http.put(url, body, this.generateRequestOptionsWithLogin())
        .subscribe(
          (response: Response) => {
            this.refreshAccessToken(response.json().data.user);
            resolve(response);
          },
          (error) => {
            this.handleExpiredTokenError(error);
            reject(error);
          }
        );
    });
  }

  deleteWithLogin(url: string): Promise<Response> {
    return new Promise<Response>((resolve, reject) => {
      this.http.delete(url, this.generateRequestOptionsWithLogin())
        .subscribe(
          (response: Response) => {
            this.refreshAccessToken(response.json().data.user);
            resolve(response);
          },
          (error) => {
            this.handleExpiredTokenError(error);
            reject(error);
          }
        );
    });
  }

  private generateRequestOptionsWithLogin(): RequestOptions {
    return new RequestOptions({
      headers: new Headers({
        'x-access-token': this.authService.getAccessToken(),
        'cache-control': 'no-cache'
      })
    });
  }

  /**
   * if error.status == 401, the token probably expires at server side
   * so logout and navigate back to login page
   *
   * @param error
   * @param reject
   */
  private handleExpiredTokenError(error) {
    if (error.status === HttpStatus.UNAUTHORIZED) {
      this.authService.logout();
      this.router.navigate(['login']);
    }
  }

  private refreshAccessToken(user) {
    if (user) {
      this.authService.setUser(user);
    }
  }
}
