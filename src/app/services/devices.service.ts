import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { HttpService } from './http/http.service';

import { HOST_URL } from '../config/config';

@Injectable()
export class DevicesService {

  constructor(private httpService: HttpService) {}

  getDevicesFromServer() {
    return new Promise<Device[]>((resolve, reject) => {
      this.httpService.getWithLogin(HOST_URL + '/device')
        .then((response: Response) => {
          resolve(response.json().data.devices ? response.json().data.devices : []);
        })
        .catch(error => reject(error));
    });
  }
}
