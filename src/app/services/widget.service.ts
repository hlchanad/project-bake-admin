import { Injectable } from '@angular/core';
import { Response } from '@angular/http';

import { HttpService } from './http/http.service';
import { HOST_URL } from '../config/config';

@Injectable()
export class WidgetService {

  constructor(private httpService: HttpService) {}

  getCountFromServer(type: string) {
    return new Promise<number>((resolve, reject) => {
      this.httpService.getWithLogin(HOST_URL + `/stat/count/${type}`)
        .then((response: Response) => {
          resolve(+response.json().data.count);
        })
        .catch((error) => (error.status === 304) ? resolve(0) : reject(error)); // NOT MODIFIED => no data
    });
  }
}
