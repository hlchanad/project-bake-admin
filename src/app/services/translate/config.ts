import { LANG_EN_NAME } from './lang-en';
import { LANG_ZH_NAME } from './lang-zh';

export const DEFAULT_LANG = LANG_ZH_NAME;
