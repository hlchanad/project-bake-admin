// import translations
import { LANG_EN_NAME, LANG_EN_TRANSLATION } from './lang-en';
import { LANG_ZH_NAME, LANG_ZH_TRANSLATION } from './lang-zh';

// all translations
export const DICTIONARY = {
  [LANG_EN_NAME]: LANG_EN_TRANSLATION,
  [LANG_ZH_NAME]: LANG_ZH_TRANSLATION,
};

export const SUPPORTED_LANGUAGES = [
  {
    display: '繁體中文',
    code: LANG_ZH_NAME
  }, {
    display: 'English',
    code: LANG_EN_NAME
  }
];
