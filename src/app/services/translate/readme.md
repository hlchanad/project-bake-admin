### Usage
1. Import `TranslateModule` to any `@NgModule` 
2. provide `TranslateService` at `AppModule`

### Reason
- Sub-module can use the `declarations` (`TranslatePipe` for now) exported from `TranslateModule`
- Providing `TranslateService` at `AppModule` let all `TranslateService` instances injected will be the same instance
