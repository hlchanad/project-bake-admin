export const LANG_ZH_NAME = 'zh';

export const LANG_ZH_SUFFIX = '_zh';

export const LANG_ZH_TRANSLATION = {
  // general
  'english': '英文',
  'chinese': '中文',
  'username': '使用者名稱',
  'password': '密碼',
  'login': '登入',
  'logout': '登出',
  'welcome': '歡迎',
  'home': '主頁',
  'recipes': '食譜',
  'devices': '裝置',
  'yes': '是',
  'no': '否',
  'okay': '好',
  'close': '關閉',
  'error': '錯誤',
  'sthWentWrong': '有些問題發生了，請稍後再試。',
  'loginFail': '登入失敗',
  'invalid_credential': '登入資料錯誤',
  'forbidden': '你沒有相關權限',
  'rememberMe': '記住我',
  'pleaseWait': '請稍候',
  'loginRequired': '必須登入以探訪該頁面',

  // breadcrumb


  // not found
  'weCannotFind': '我們找不到你的',
  'cake': '蛋糕',
  'fullStop': '。',

  // hl-table
  'create': '新記錄',
  'first': '第一頁',
  'previous': '上一頁',
  'next': '下一頁',
  'last': '最後一頁',
  'all': '全部',
  'filterByAll': '以 所有欄目 篩選 ...',
  'filterById': '以 ID 篩選 ...',
  'filterByTitle': '以 名稱 篩選 ...',
  'filterByDesc': '以 簡介 篩選 ...',
  'filterBySize': '以 份量 篩選 ...',
  'filterByThumbnail': '以 預覽圖 篩選 ...',
  'filterByParts': '以 段落 篩選 ...',
  'filterByImages': '以 圖片庫 篩選 ...',
  'filterByCreatedAt': '以 建立時間 篩選 ...',
  'filterByUpdatedAt': '以 更改時間 篩選 ...',
  'filterByStatus': '以 狀態 篩選 ...',
  'filterByType': '以 平台 篩選 ...',
  'filterByToken': '以 識別碼 篩選 ...',

  // recipes columns
  'id': 'ID',
  'title': '名稱',
  'desc': '簡介',
  'tags': '標籤',
  'size': '份量',
  'thumbnail': '預覽圖',
  'parts': '部分',
  'images': '圖片庫',
  'createdAt': '建立時間',
  'updatedAt': '更改時間',
  'status': '狀態',
  'active': '開放',
  'suspended': '暫停',
  'deleted': '已刪除',
  'ingredients': '材料',
  'steps': '步驟',
  'ingredient-name': '食材名稱',
  'ingredient-quantity': '數量',
  'ingredient-unit': '單位',
  'step-line': '做法',
  'step-autoNextStep': '自動下一步',
  'step-duration': '時間 (秒數)',

  // recipe page
  'edit': '編輯',
  'view': '瀏覽',
  'delete': '刪除',
  'suspend': '暫停',
  'activate': '開放',
  'cancel': '取消',
  'complete': '完成',
  'confirm': '確認',
  'recipe-page-sureDelete': '你確定要刪除這份食譜？',
  'recipe-page-sureSuspend': '你確定要暫定這份食譜？',
  'recipe-page-sureCancel': '你確定要取消改動？',
  'noSuchRecipe': '這份食譜不存在或已被刪除',
  'sec': '秒',
  'exceedFileSize': '圖片過大！請上載一張比較小的圖片',
  'createRecipe': '新食譜',

  // devices columns
  'type': '平台',
  'token': '識別碼'
};
