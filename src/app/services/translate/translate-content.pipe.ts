import { Injectable, Pipe, PipeTransform } from '@angular/core';

import { TranslateService } from './translate.service';

@Injectable()
@Pipe({
  name: 'translateContent',
  pure: false
})
export class TranslateContentPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {}

  transform(object: any, key: string, type: string = 'text') {

    const suffix = this.translateService.getCurrentLangSuffix();
    if (!object[key + suffix]) {
      switch (type) {
        case 'text':  return 'no this key: ' + key + suffix;
        case 'array': return [];
      }
      // throw new Error(key + suffix + ' not found in provided object');
    }

    return object[key + suffix];
  }
}
