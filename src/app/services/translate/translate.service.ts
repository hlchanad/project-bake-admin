import { EventEmitter, Injectable } from '@angular/core';

import { LocalStorageService } from 'ngx-store';

import { SUPPORTED_LANGUAGES, DICTIONARY } from './translation';
import { DEFAULT_LANG } from './config';
import { LANG_ZH_NAME, LANG_ZH_SUFFIX } from 'app/services/translate/lang-zh';
import { LANG_EN_NAME, LANG_EN_SUFFIX } from './lang-en';

@Injectable()
export class TranslateService {

  currentLang: string;
  dictionary: any;
  supportedLanguages: { display: string, code: string }[];
  languageChanged = new EventEmitter<void>();

  constructor(private localStorageService: LocalStorageService) {
    this.initLang();
    this.dictionary = DICTIONARY;
    this.supportedLanguages = SUPPORTED_LANGUAGES;
  }

  initLang() {
    this.currentLang = this.localStorageService.get('pickedLang') || DEFAULT_LANG;
  }

  getCurrentLanguage() {
    return this.supportedLanguages.find(
      (language) => {
        return language.code === this.currentLang;
      }
    );
  }

  getCurrentLangSuffix() {
    switch (this.currentLang) {
      case LANG_EN_NAME: return LANG_EN_SUFFIX;
      case LANG_ZH_NAME: return LANG_ZH_SUFFIX;
    }
  }

  getSupportedLanguages() {
    return this.supportedLanguages;
  }

  use(lang: string) {
    if (!this.isLangSupported(lang)) {
      return ; // ignore if not allowed lang choice
    }
    this.currentLang = lang;
    this.localStorageService.set('pickedLang', lang);
    this.languageChanged.emit();
  }

  translate(key: string, useLang: string = null): string {
    const lang = useLang === null ? this.currentLang : useLang;

    if (this.dictionary[lang][key]) {
      return this.dictionary[lang][key];
    } else {
      return key;
    }
  }

  private isLangSupported(lang: string) {
    for (let i = 0; i < this.supportedLanguages.length; i++) {
      if (this.supportedLanguages[i].code === lang) {
        return true;
      }
    }
    return false;
  }
}
