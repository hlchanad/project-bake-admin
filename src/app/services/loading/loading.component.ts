import { Component } from '@angular/core';
import { TranslateService } from '../translate/translate.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss']
})
export class LoadingComponent {

  title: string = this.translateService.translate('pleaseWait');

  constructor(private translateService: TranslateService) {}
}
