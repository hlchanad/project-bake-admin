import { NgModule } from '@angular/core';

import { LoadingComponent } from './loading.component';
import { LoadingService } from './loading.service';
import { TranslateModule } from '../translate/translate.module';

@NgModule({
  declarations: [ LoadingComponent ],
  entryComponents: [ LoadingComponent ],
  imports: [ TranslateModule ],
  providers: [ LoadingService ]
})
export class LoadingModule {}
