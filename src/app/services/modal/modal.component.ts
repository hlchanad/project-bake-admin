import { Component, EventEmitter, Input, Output } from '@angular/core';

import { TranslateService } from '../translate/translate.service';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
  @Input('showConfirmButton') showConfirm = false;

  @Input('confirmText') confirmText = this.translateService.translate('okay');
  @Input('cancelText')  cancelText = this.translateService.translate('close');

  @Input('modalTitle')   title = this.translateService.translate('error');
  @Input('modalContent') content = this.translateService.translate('sthWentWrong');

  @Output('onConfirm') onConfirm = new EventEmitter<void>();
  @Output('onDismiss') onDismiss = new EventEmitter<void>();

  constructor(private translateService: TranslateService) {}

  setTitle(title: string) {
    this.title = title;
  }

  setContent(content: string) {
    this.content = content;
  }

  setConfirmText(text: string) {
    this.confirmText = text;
  }

  setCancelText(text: string) {
    this.cancelText = text;
  }

  showConfirmButton() {
    this.showConfirm = true;
  }

  getOnDismissPublisher() {
    return this.onDismiss;
  }

  getOnConfirmPublisher() {
    return this.onConfirm;
  }
}
