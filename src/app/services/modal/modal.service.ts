import { Injectable, EventEmitter } from '@angular/core';

import { BsModalRef, BsModalService } from 'ngx-bootstrap';

import { ModalComponent } from './modal.component';

interface ModalOption {
  title?: string;
  content?: string;
  confirmButtonText?: string;
  cancelButtonText?: string;
  showConfirm?: boolean;
};

@Injectable()
export class ModalService {

  modalRef: BsModalRef;

  dismissSubscription: any;
  confirmSubscription: any;

  onConfirm = new EventEmitter<void>();

  constructor(private bsModalService: BsModalService) {}

  show(option: ModalOption = {}) {
    this.modalRef = this.bsModalService.show(ModalComponent);

    const modalContent = <ModalComponent> this.modalRef.content;
    if (option.title) { modalContent.setTitle(option.title); }
    if (option.content) { modalContent.setContent(option.content); }
    if (option.confirmButtonText) { modalContent.setConfirmText(option.confirmButtonText); }
    if (option.cancelButtonText) { modalContent.setCancelText(option.cancelButtonText); }
    if (option.showConfirm) { modalContent.showConfirmButton(); }

    this.dismissSubscription = modalContent.getOnDismissPublisher()
    .subscribe(() => {
      this.modalRef.hide();
      this.dismissSubscription.unsubscribe();
    });

    this.confirmSubscription = modalContent.getOnConfirmPublisher()
    .subscribe(() => {
      this.onConfirm.emit();
      this.modalRef.hide();
      this.confirmSubscription.unsubscribe();
    });
  }
}
