import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule as NgxModalModule } from 'ngx-bootstrap';

import { ModalService } from './modal.service';
import { ModalComponent } from 'app/services/modal/modal.component';

@NgModule({
  declarations: [ ModalComponent ],
  imports: [
    CommonModule,
    NgxModalModule.forRoot()
  ],
  entryComponents: [ ModalComponent ],
  providers: [ ModalService ]
})
export class ModalModule {}
