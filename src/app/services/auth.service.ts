import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { SessionStorageService } from 'ngx-store';
import * as HttpStatus from 'http-status-codes';

import { HOST_URL } from '../config/config';
import UserConstants from '../constants/user';

interface User {
  _id: string;
  username: string;
  role: string; // 'admin'/ 'normal'
  accessToken: string;
  expireAt: Date;
}

@Injectable()
export class AuthService {

  private user: User;

  // cannot inject HttpService due to circular injection, use native Http instead
  constructor(private http: Http,
              private sessionStorageService: SessionStorageService) {

    this.initUser();
  }

  login(username: string, password: string) {
    return new Promise<void>((resolve, reject) => {
      this.http.post(HOST_URL + '/user/login', { username: username, password: password })
        .subscribe(
          (response: Response) => {
            const data = response.json().data;

            if (data.user.role === UserConstants.UserRole.Normal) {
              reject({ status: HttpStatus.FORBIDDEN });
            } else {
              this.setUser(data.user);
              resolve();
            }
          },
          (error) => {
            this.logout();
            reject(error);
          }
        );
      });
  }

  logout() {
    this.setUser(null);
  }

  initUser() {
    this.user = this.sessionStorageService.get('loggedUser');
    if (this.user && this.isUserExpired()) {
      this.logout();
    }
  }

  setUser(user: User) {
    this.user = user;
    this.sessionStorageService.set('loggedUser', user);
  }

  getAuthenticatedUser() {
    return new Object(this.user);
  }

  getAccessToken() {
    return this.user.accessToken;
  }

  isAuthenticated() {
    if (this.user && this.isUserExpired()) {
      this.logout();
    }
    return this.user !== null;
  }

  isUserExpired() {
    return (new Date(this.user.expireAt)).getTime() < (new Date()).getTime();
  }
}
