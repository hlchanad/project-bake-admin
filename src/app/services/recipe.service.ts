import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import { HOST_URL } from '../config/config';
import { Recipe } from '../recipes/models/recipe';
import { AuthService } from './auth.service';
import { HttpService } from './http/http.service';

@Injectable()
export class RecipeService {

  recipes: Recipe[] = [];

  constructor(private http: Http,
              private httpService: HttpService,
              private authService: AuthService) {}

  getRecipes() {
    return new Promise<Recipe[]>((resolve, reject) => {
      if (this.recipes.length === 0) {
        this.getRecipesFromServer()
          .then(() => resolve(this.recipes.slice()))
          .catch((error) => reject(error));
      } else {
        resolve(this.recipes.slice());
      }
    });
  }

  getRecipeById(id: string) {
    return new Promise<Recipe>((resolve, reject) => {
      let index = this.recipes.findIndex((recipe: Recipe) => recipe._id === id);

      if (this.recipes.length >= 0 && index >= 0) {
        resolve(this.recipes.slice()[index]);
      } else {
        this.getRecipesFromServer()
          .then((recipes: Recipe[]) => {
            index = this.recipes.findIndex((recipe: Recipe) => recipe._id === id);
            if (index >= 0) {
              resolve(this.recipes.slice()[index]);
            } else {
              reject('no_such_recipe');
            }
          })
          .catch((error) => reject(error));
      }
    });
  }

  getRecipeByIdFromServer(recipeId: string) {
    return new Promise<Recipe>((resolve, reject) => {
      this.httpService.getWithLogin(HOST_URL + '/recipe/' + recipeId)
        .then((response: Response) => resolve(response.json().data.recipe))
        .catch((error) => reject(error));
    });
  }

  getRecipesFromServer() {
    return new Promise<Recipe[]>((resolve, reject) => {
      this.httpService.getWithLogin(HOST_URL + '/recipe')
        .then((response: Response) => {
          this.recipes = response.json().data.recipes ? response.json().data.recipes : [];
          resolve(this.recipes);
        })
        .catch((error) => (error.status === 304) ? resolve([]) : reject(error)); // NOT MODIFIED => no data
    });
  }

  activateRecipe(recipeId) {
    return new Promise<void>((resolve, reject) => {
      this.httpService.putWithLogin(HOST_URL + '/recipe/' + recipeId + '/activate', {})
        .then((response: Response) => resolve())
        .catch((error) => reject());
    });
  }

  suspendRecipe(recipeId) {
    return new Promise<void>((resolve, reject) => {
      this.httpService.putWithLogin(HOST_URL + '/recipe/' + recipeId + '/suspend', {})
        .then((response: Response) => resolve())
        .catch((error) => reject());
    });
  }

  deleteRecipe(recipeId) {
    return new Promise<void>((resolve, reject) => {
      this.httpService.deleteWithLogin(HOST_URL + '/recipe/' + recipeId)
        .then((response: Response) => resolve())
        .catch((error) => reject());
    });
  }

  /**
   *
   * @param recipe
   * @param thumbnail base64 encoded
   * @param images base64 encoded images array
   */
  createRecipe(recipe: Recipe, thumbnail: any, images: any[]) {
    return new Promise<string>((resolve, reject) => {
      const data = {
        thumbnail: thumbnail,
        images: images,
        recipe: recipe
      };

      this.httpService.postWithLogin(HOST_URL + '/recipe', data)
        .then((response: Response) => resolve(response.json().data._id))
        .catch((error) => reject(error) );
    });
  }

  editRecipe(id: string, recipe: Recipe, thumbnail: any, images: any[]) {
    return new Promise<void>((resolve, reject) => {
      const data = {
        thumbnail: thumbnail,
        images: images,
        recipe: recipe
      };

      this.httpService.putWithLogin(HOST_URL + '/recipe/' + id, data)
        .then((response: Response) => resolve())
        .catch((error) => reject(error));
    });
  }
}
