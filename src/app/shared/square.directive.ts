import { Directive, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appSquare]'
})
export class SquareDirective implements OnInit {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    const width = window.getComputedStyle(this.elementRef.nativeElement).width;
    this.renderer.setStyle(this.elementRef.nativeElement, 'height', width);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const width = window.getComputedStyle(this.elementRef.nativeElement).width;
    this.renderer.setStyle(this.elementRef.nativeElement, 'height', width);
  }

}
