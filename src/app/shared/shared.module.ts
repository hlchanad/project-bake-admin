import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SquareDirective } from './square.directive';
import { ModalModule } from '../services/modal/modal.module';
import { LoadingModule } from '../services/loading/loading.module';

@NgModule({
  declarations: [
    SquareDirective
  ],
  imports: [
    CommonModule,
    ModalModule,
    LoadingModule
  ],
  exports: [
    SquareDirective
  ]
})
export class SharedModule {}
