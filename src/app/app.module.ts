import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// ************************** ngx by valor **********************************
import {
  AccordionModule as NgxAccordionModule,
  ButtonsModule as NgxButtonsModule,
  BsDropdownModule as NgxDropdownModule,
  TabsModule as NgxTabsModule
} from 'ngx-bootstrap';
import { WebStorageModule as NgxWebStorageModule } from 'ngx-store';
// **************************************************************************

// ********************** angular-modal-gallery *****************************
import 'hammerjs';
import 'mousetrap';
import { ModalGalleryModule } from 'angular-modal-gallery';
// **************************************************************************

import { AppRoutingModule } from './app-routing.module';
import { TranslateModule } from './services/translate/translate.module';
import { RecipesModule } from './recipes/recipes.module';
import { HomeModule } from './home/home.module';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { NotFoundComponent } from './not-found/not-found.component';

import { AuthService } from 'app/services/auth.service';
import { AuthGuard } from './services/auth-guard';
import { TranslateService } from './services/translate/translate.service';
import { HttpService } from './services/http/http.service';
import { BreadcrumbService } from './services/breadcrumb.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    BreadcrumbComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    RecipesModule,
    HomeModule,
    TranslateModule,
    SharedModule,
    NgxAccordionModule.forRoot(),
    NgxButtonsModule.forRoot(),
    NgxDropdownModule.forRoot(),
    NgxTabsModule.forRoot(),
    NgxWebStorageModule,
    ModalGalleryModule.forRoot()
  ],
  providers: [
    AuthService,
    AuthGuard,
    TranslateService,
    HttpService,
    BreadcrumbService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
