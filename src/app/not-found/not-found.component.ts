import { Component } from '@angular/core';
import { BreadcrumbService } from '../services/breadcrumb.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  missingIngredientColor = this.getRandomColor();

  constructor(private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.disableBreadcrumbs();
  }

  getRandomColor() {
    const colors = [
      '#fb4a4a',
      '#fd4cb2',
      '#d256ff',
      '#8456ff',
      '#569dff',
      '#56dcff',
      '#56ffd4',
      '#56ff58',
      '#9bff56',
      '#e9ff56',
      '#ffc756',
      '#ff6a56'
    ];
    return colors[Math.floor(Math.random() * colors.length)];
  }
}
