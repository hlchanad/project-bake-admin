interface Device {
  _id: string;
  type: string;
  token: string;
}
