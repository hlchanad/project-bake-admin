import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HLTableModule } from '../hl-table/hl-table.module';
import { TranslateModule } from 'app/services/translate/translate.module';
import { DevicesRoutingModule } from './devices-routing.module';

import { DevicesService } from '../services/devices.service';

import { DevicesComponent } from './devices.component';


@NgModule({
  declarations: [
    DevicesComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    HLTableModule,
    DevicesRoutingModule
  ],
  providers: [
    DevicesService
  ]
})
export class DevicesModule {}
