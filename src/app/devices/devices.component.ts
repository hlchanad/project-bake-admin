import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { BreadcrumbService } from '../services/breadcrumb.service';
import { LANG_ZH_NAME } from 'app/services/translate/lang-zh';
import { LANG_EN_NAME } from '../services/translate/lang-en';
import { TranslateService } from '../services/translate/translate.service';
import { LoadingService } from '../services/loading/loading.service';
import { ModalService } from '../services/modal/modal.service';
import { DevicesService } from '../services/devices.service';
import { HLTableComponent } from '../hl-table/hl-table.component';
import { ColumnSettings, ColumnType } from '../hl-table/models/column-setting.model';

@Component({
  selector: 'app-devices',
  templateUrl: 'devices.component.html',
  styleUrls: ['devices.component.scss']
})
export class DevicesComponent implements OnInit, OnDestroy {

  @ViewChild(HLTableComponent) hlTable: HLTableComponent;
  langChanges: any;

  constructor(private breadcrumbService: BreadcrumbService,
              private devicesService: DevicesService,
              private loadingService: LoadingService,
              private modalService: ModalService,
              private translateService: TranslateService) {}

  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{
      title: this.translateService.translate('devices', LANG_EN_NAME),
      title_zh: this.translateService.translate('devices', LANG_ZH_NAME),
      url: 'devices'
    }])

    this.loadingService.show();
    this.initHLTable();
    this.langChanges = this.translateService.languageChanged.subscribe(() => {
      this.initHLTable(true);
    });
  }

  ngOnDestroy() {
    this.langChanges.unsubscribe();
  }

  initHLTable(langChanged = false) {
    this.devicesService.getDevicesFromServer()
      .then((devices: Device[]) => {
        this.hlTable.initData(this.getColumnSettings(), devices, langChanged);
      })
      .catch((error) => this.modalService.show())
      .then(() => this.loadingService.hide());
  }

  private getColumnSettings(): ColumnSettings {
    // --- Setting for generating hl-table columns ---
    // this column setting's keys should be identical to Recipe object's keys
    return {
      _id: {
        name: '_id',
        title: this.translateService.translate('id'),
        type: ColumnType.ObjectId,
        visible: false,
        filterable: false,
        sortable: true
      },
      type: {
        name: 'type',
        title: this.translateService.translate('type'),
        type: ColumnType.Dropdown,
        choices: [
          { value: 'Android', name: 'Android' },
          { value: 'iOS', name: 'iOS' },
          { value: 'Web', name: 'Web' }
        ],
        visible: true,
        filterable: true,
        sortable: true
      },
      token: {
        name: 'token',
        title: this.translateService.translate('token'),
        type: ColumnType.Text,
        visible: true,
        filterable: true,
        sortable: true
      }
    };
  }
}
