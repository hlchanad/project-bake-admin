import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { BreadcrumbService } from '../services/breadcrumb.service';
import { WidgetService } from '../services/widget.service';
import { LoadingService } from '../services/loading/loading.service';
import { ModalService } from '../services/modal/modal.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  numberOfRecipes = 0;
  numberOfDevices = 0;

  constructor(private router: Router,
              private breadcrumbService: BreadcrumbService,
              private modalService: ModalService,
              private loadingService: LoadingService,
              private widgetService: WidgetService) {

    this.breadcrumbService.setBreadcrumb([]);

    this.loadingService.show();

    Promise.all([
      this.widgetService.getCountFromServer('recipe')
        .then(count => this.numberOfRecipes = count),

      this.widgetService.getCountFromServer('device')
        .then(count => this.numberOfDevices = count)
    ])
      .catch(() => this.modalService.show())
      .then(() => this.loadingService.hide());
  }

  onClickWidgetNavigate(uri: string) {
    this.router.navigate([uri]);
  }
}
