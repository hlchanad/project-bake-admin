import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-number-widget',
  templateUrl: 'number-widget.component.html',
  styleUrls: ['number-widget.component.scss']
})
export class NumberWidgetComponent implements OnInit {

  static DEFAULT_COLOR = 'blue';

  @Input() color = NumberWidgetComponent.DEFAULT_COLOR;
  @Input() cursorStyle = 'auto';
  @Input() icon = 'fa-area-chart';
  @Input() number = 0;
  @Input() desc: string;

  ngOnInit() {
    const allowedColors = ['blue', 'red', 'green', 'purple'];

    if (!this.color || allowedColors.indexOf(this.color) < 0) {
      this.color = NumberWidgetComponent.DEFAULT_COLOR;
    }
  }

}
