import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from '../services/modal/modal.module';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '../services/translate/translate.module';

import { WidgetService } from '../services/widget.service';

import { HomeComponent } from './home.component';
import { NumberWidgetComponent } from './components/number-widget/number-widget.component';

@NgModule({
  declarations: [
    HomeComponent,
    NumberWidgetComponent
  ],
  imports: [
    CommonModule,
    ModalModule,
    SharedModule,
    TranslateModule
  ],
  providers: [
    WidgetService
  ]
})
export class HomeModule {}
