import { ChangeDetectionStrategy, Component } from '@angular/core';

import { BreadcrumbService } from '../services/breadcrumb.service';
import { Breadcrumb } from './breadcrumb.model';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent {

  private breadcrumbs: Breadcrumb[] = [];

  constructor(private breadcrumbService: BreadcrumbService) {}

  getBreadcrumbs() {
    this.breadcrumbs = this.breadcrumbService.getBreadcrumbs();
    return this.breadcrumbs;
  }
}
