export interface Breadcrumb {
  title: string;
  title_zh: string;
  url: string;
}
