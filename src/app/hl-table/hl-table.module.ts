import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PaginationModule } from 'ngx-bootstrap';
import { Ng2TableModule } from 'ng2-table';

import { HLTableComponent } from './hl-table.component';
import { HLTableService } from './hl-table.service';
import { TranslateModule } from '../services/translate/translate.module';

@NgModule({
  declarations: [
    HLTableComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TranslateModule
  ],
  exports: [
    HLTableComponent
  ],
  providers: [
    HLTableService
  ]
})
export class HLTableModule {}
