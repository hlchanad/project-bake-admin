import { Component, EventEmitter, Input, Output, Renderer2, ViewEncapsulation } from '@angular/core';
import { HLTableService } from './hl-table.service';
import { ColumnDropdownChoices, ColumnSettings, ColumnType } from './models/column-setting.model';


@Component({
  selector: 'app-hl-table',
  templateUrl: './hl-table.component.html',
  styleUrls: ['./hl-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HLTableComponent {

  // more about valor/ng-table, please go to
  // - http://valor-software.com/ng2-table/
  // - https://github.com/valor-software/ng2-table
  rows: any[] = [];         // to be shown in ng-table
  columns: any[] = [];      // columns setting for ng-table,
                            // filtering/ sorting options will be applied here by ng-table as well
  page = 1;                 // page number of current row
  itemsPerPage = 10;        // number of items in one page
  maxSize = 3;              // number of the displaying pages before ... (pagination)
  numPages = 1;             // number of pages, will be updated for pagination
  length = 0;               // number of records, will be updated for filtering
  config: any = {};         // config for ng-table,
                            // global filtering/ sorting options will be applied here by ng-table as well
  private data: any[] = []; // real data store here

  // INPUTS
  @Input() allowCreation = true;

  // OUTPUTS
  @Output('cellClicked') cellClicked = new EventEmitter<{ row: any, column: string }>();
  @Output('onCreate') onCreate = new EventEmitter<void>();

  columnSettings: ColumnSettings;

  constructor(private hlTableService: HLTableService,
              private render: Renderer2) {}

  initData(columnSettings: ColumnSettings, rawData: any, langChanged = false) {

    this.columnSettings = columnSettings;
    this.columns = this.hlTableService.setColumnsForNgTable(columnSettings);
    this.config  = this.hlTableService.setConfigForNgTable(this.columns);
    this.data    = this.hlTableService.generateDataForNgTable(rawData, columnSettings);

    this.onChangeTable(this.config);

    if (langChanged) {
      this.clearFilter();
    }
  }

  changePage(page: any, data: any = this.data): any[] {

    // return original data if paging is not allowed
    if (!this.config.paging) { return data; }

    // slice the data for paging
    const start = (page.page - 1) * page.itemsPerPage,
          end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  changeSort(data: any, config: any): any {

    // return original data if sorting is not allowed
    if (!config.sorting) { return data; }

    const columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    // get sort by "columnName" with "sort" order
    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    // return original data if anything goes wrong
    if (!columnName) { return data; }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  changeFilter(data: any, config: any): any {

    // return original data if filtering is not allowed
    if (!config.filtering) { return data; }

    let filteredData: any[] = data;

    // filter data by "filter input" on the hl-table
    this.columns.forEach((column: any) => {
      if (!column.filtering) { return ; }

      filteredData = filteredData.filter((item: any) => {
        switch (this.columnSettings[column.name].type) {
          case ColumnType.Text:
            return item[column.name].toLowerCase().match(column.filtering.filterString.toLowerCase());

          case ColumnType.Dropdown:
            const choice = this.columnSettings[column.name].choices.find(
              (choiceEl: ColumnDropdownChoices) => choiceEl.value.toString() === column.filtering.filterString.toString() );
            return column.filtering.filterString === HLTableService.DROPDOWN_ALL || item[column.name].match(choice.name);
        }
      });
    });

    // filter data once more by "global filter" (simply comparing string)
    const tempArray: any[] = [];
    filteredData.forEach((item: any) => {
      let searchSuccessInItem = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().toLowerCase().match(config.filtering.filterString.toLowerCase())) {
          searchSuccessInItem = true;
        }
      });
      if (searchSuccessInItem) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  onChangeTable(config: any, page: any = { page: this.page, itemsPerPage: this.itemsPerPage }): any {

    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }
    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    const filteredData = this.changeFilter(this.data, this.config),
          sortedData = this.changeSort(filteredData, this.config);

    this.rows = page ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;

    this.changeFilterInput();
  }

  /**
   * clear all filter, should be used after changing admin panel ui language
   */
  private clearFilter() {
    const hlTable = document.querySelector('app-hl-table'),
          inputs = hlTable.getElementsByTagName('input'),
          selects = hlTable.getElementsByTagName('select');
    for (let i = 0; i < inputs.length; i++) {
      inputs[i].value = '';
    }
    for (let i = 0; i < selects.length; i++) {
      selects[i].value = HLTableService.DROPDOWN_ALL;
    }
  }

  /**
   * change some filter from input -> sth else
   * for example: dropdown
   *
   * @returns {Promise<void>}
   */
  private changeFilterInput(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      const columnKeys = Object.keys(this.columnSettings);
      columnKeys.forEach((columnKey: string) => {

        const index = this.columns.findIndex((column: any) => column.name === columnKey);
        if (index < 0) { return ; } // not visible -> no need to do anything

        const columnSetting = this.columnSettings[columnKey];

        switch (columnSetting.type) {
          case ColumnType.Dropdown:
            setTimeout(() => {

              const query = 'app-hl-table ng-table > table > tbody > tr:nth-child(1) > td:nth-child(' + (index + 1) + ')',
                    inputCell = document.querySelector(query);

              const selectElement = this.hlTableService.generateSelectFilter(columnSetting);

              this.render.listen(selectElement, 'change', (event) => {
                this.onChangeDropdownFilter(event);
              });

              // remove existing input filter/ select filter
              const exitingInputFilter  = inputCell.querySelector('input'),
                    existingSelectFilter = inputCell.querySelector('select');

              if (exitingInputFilter) { exitingInputFilter.remove(); }

              if (existingSelectFilter) {
                // set existing options to newly created select and remove existing one
                const optionIndex = existingSelectFilter.selectedIndex,
                      optionElement = <HTMLOptionElement>existingSelectFilter.querySelector('option:nth-child(' + (optionIndex + 1) + ')');
                selectElement.value = optionElement.value;
                existingSelectFilter.remove();
              }

              // append my custom select filter
              inputCell.appendChild(selectElement);

              resolve();
            }, 0);
            break;
        }
      });
    });
  }

  onChangeDropdownFilter(event: any) {
    const selectElement = event.target,
          id = selectElement.id.substring(0, selectElement.id.length - '_status'.length),
          optionIndex = selectElement.selectedIndex,
          optionElement = <HTMLOptionElement>selectElement.querySelector('option:nth-child(' + (optionIndex + 1) + ')'),
          columnIndex = this.columns.findIndex((column: any) => column.name === id);

    this.columns[columnIndex].filtering.filterString = optionElement.value;
    this.onChangeTable(this.config);
  }

  onCellClick(event: { row: any, column: string }) {
    this.cellClicked.emit(event);
  }

  onClickedCreateButton() {
    this.onCreate.emit();
  }
}
