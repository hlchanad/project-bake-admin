export enum ColumnType {
  ObjectId = 1,
  Text = 2,
  Date = 3,
  Photo = 4,
  Array = 5,
  Dropdown = 6
}

export interface ColumnDropdownChoices {
  name: string;
  value: any;
}

export interface ColumnSetting {
  name: string;
  title: string;
  type: ColumnType;
  visible: boolean;
  filterable: boolean;
  sortable: boolean;

  charLimit?: number;                // only useful when type = ColumnType.Text
  choices?: ColumnDropdownChoices[]; // only useful when type = ColumnType.Dropdown
}

export interface ColumnSettings {
  [key: string]: ColumnSetting;
}
