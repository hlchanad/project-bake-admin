import { Injectable } from '@angular/core';

import { ColumnDropdownChoices, ColumnSetting, ColumnSettings, ColumnType } from './models/column-setting.model';
import { TranslateService } from '../services/translate/translate.service';

@Injectable()
export class HLTableService {

  static DROPDOWN_ALL = 'All';

  constructor(private translateService: TranslateService) {}

  generateSelectFilter(columnSetting: ColumnSetting) {
    const selectElement = document.createElement('select');
    selectElement.id = columnSetting.name + '_filter';
    selectElement.className = 'form-control';
    selectElement.style.width = 'auto';

    const defaultOptionElement = document.createElement('option');
    defaultOptionElement.value = HLTableService.DROPDOWN_ALL;
    defaultOptionElement.text  = this.translateService.translate('all');
    selectElement.appendChild(defaultOptionElement);

    columnSetting.choices.forEach((choice: ColumnDropdownChoices) => {
      const optionElement = document.createElement('option');
      optionElement.value = choice.value;
      optionElement.text  = choice.name;
      selectElement.appendChild(optionElement);
    });

    return selectElement;
  }

  setConfigForNgTable(columns: any[]): any {
    return {
      paging: true,
      sorting: { columns: columns },
      filtering: { filterString: '' }     // global filter applies here
    };
  }

  setColumnsForNgTable(columnSettings: ColumnSettings): any[] {
    const columns: any[] = [];

    const columnSettingKeys = Object.keys(columnSettings);
    for (let i = 0; i < columnSettingKeys.length; i++) {

      const key = columnSettingKeys[i],
            columnSetting: ColumnSetting = columnSettings[key];

      if (!columnSetting.visible) { continue; } // skip setting columns for ng-hl-table if !visible

      const column = {
        name: columnSetting.name,
        title: columnSetting.title,
        sort: columnSetting.sortable ? '' : false
      };

      if (columnSetting.filterable) {
        const filterKey = 'filterBy' + columnSetting.name.substring(0, 1).toUpperCase() + columnSetting.name.substring(1),
              filterString = columnSetting.type === ColumnType.Dropdown ? HLTableService.DROPDOWN_ALL : '';
        column['filtering'] = {
          filterString: filterString,
          placeholder: this.translateService.translate(filterKey)
        };
      }

      columns.push(column);
    }

    return columns;
  }

  generateDataForNgTable(sourceData: any[], columnSettings: ColumnSettings): any[] {
    const returnData = [];
    for (let i = 0; i < sourceData.length; i++) {
      const item: any = {};
      Object.assign(item, sourceData[i]); // get a fresh copy of item, to prevent accidentally modified sourceData
      const itemKeys = Object.keys(item);

      for (let j = 0; j < itemKeys.length; j++) {
        const key = itemKeys[j];
        item[key] = this.parseField(item[key], columnSettings[key]);
      }

      returnData.push(item);
    }
    return returnData;
  }

  private parseField(value: any, setting: ColumnSetting): string {
    switch (setting.type) {
      case ColumnType.Array:
        return JSON.stringify(value);

      case ColumnType.Date:
        return new Date(value).toUTCString();

      case ColumnType.Dropdown:
        for (let k = 0; k < setting.choices.length; k++) {
          if (setting.choices[k].value === +value) {
            return setting.choices[k].name;
          }
        }
        return value; // if cannot match value by some accidents

      case ColumnType.ObjectId:
        return value;

      case ColumnType.Photo:
        return '<div class="img-table-cell"><img class="img-fluid img-thumbnail" src="' + value.imageUrl + '"></div>';

      case ColumnType.Text:
        if (setting.charLimit && value.length > setting.charLimit) {
          return value.substr(0, setting.charLimit) + ' ...';
        } else {
          return value;
        }

      default:
        return value;
    }
  }

}
