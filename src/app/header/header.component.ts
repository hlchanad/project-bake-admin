import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { TranslateService } from '../services/translate/translate.service';
import { LANG_ZH_NAME } from '../services/translate/lang-zh';
import { LANG_EN_NAME } from '../services/translate/lang-en';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  menuOpened = false;

  constructor(private router: Router,
              private authService: AuthService,
              private translateService: TranslateService) {
    this.onRouteChanged();
  }

  toggleMenu() {
    this.menuOpened = !this.menuOpened;
  }

  onRouteChanged() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd && this.menuOpened) {
        this.toggleMenu();
      }
    });
  }

  isChinese() {
    return this.translateService.currentLang === LANG_ZH_NAME;
  }

  toggleLang() {
    if (this.isChinese()) {
      this.translateService.use(LANG_EN_NAME);
    } else {
      this.translateService.use(LANG_ZH_NAME);
    }
  }

  isLogggedIn() {
    return this.authService.isAuthenticated();
  }

  onLogout() {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}
