import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LocalStorageService } from 'ngx-store/dist';
import * as HttpStatus from 'http-status-codes';

import { AuthService } from '../services/auth.service';
import { TranslateService } from '../services/translate/translate.service';
import { ModalService } from '../services/modal/modal.service';
import { BreadcrumbService } from '../services/breadcrumb.service';
import { LoadingService } from '../services/loading/loading.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm: FormGroup;

  constructor(private authService: AuthService,
              private router: Router,
              private modalService: ModalService,
              private loadingService: LoadingService,
              private translateService: TranslateService,
              private localStorageService: LocalStorageService,
              private breadcrumbService: BreadcrumbService) {

    if (this.authService.isAuthenticated()) {
      this.router.navigate(['home']);
    }

    this.breadcrumbService.disableBreadcrumbs();
    this.initForm();
  }

  onLogin() {
    this.loadingService.show();
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password)
      .then(() => {
        this.loadingService.hide()

        // handle some localStorage thing for UX
        const username = this.loginForm.value.rememberMe ? this.loginForm.value.username : null;
        this.localStorageService.set('username', username);
        this.localStorageService.set('rememberMe', this.loginForm.value.rememberMe);

        this.router.navigate(['home']);
      })
      .catch((error) => {
        this.loadingService.hide()

        switch (+error.status) {
          case HttpStatus.UNAUTHORIZED:
            this.modalService.show({
              title: this.translateService.translate('loginFail'),
              content: this.translateService.translate('invalid_credential')
            });
            break;

          case HttpStatus.FORBIDDEN:
            this.modalService.show({
              title: this.translateService.translate('loginFail'),
              content: this.translateService.translate('forbidden')
            });
            break;

          default:
            this.modalService.show({
              content: this.translateService.translate('sthWentWrong')
            });
            break;
        }
      });
  }

  initForm() {
    const rememberMe = this.localStorageService.get('rememberMe') || false,
          username = rememberMe ? this.localStorageService.get('username') : null;

    this.loginForm = new FormGroup({
      username: new FormControl(username, Validators.required),
      password: new FormControl(null, Validators.required),
      rememberMe: new FormControl(rememberMe, Validators.required)
    });
  }

}
