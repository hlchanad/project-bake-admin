import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { RecipeService } from '../../services/recipe.service';
import { Recipe } from '../models/recipe';
import { Image } from '../models/image';
import { Part } from '../models/part';
import { Ingredient } from '../models/ingredient';
import { Step } from '../models/step';
import { ModalService } from '../../services/modal/modal.service';
import { LoadingService } from '../../services/loading/loading.service';
import { TranslateService } from '../../services/translate/translate.service';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { Breadcrumb } from '../../breadcrumb/breadcrumb.model';
import { LANG_EN_NAME } from '../../services/translate/lang-en';
import { LANG_ZH_NAME } from '../../services/translate/lang-zh';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.scss']
})
export class RecipeEditComponent implements OnInit {

  static MAX_IMAGES = 10;
  static MAX_IMAGE_SIZE = 0.5 * 1024; // unit: KB

  _id: string;
  recipe: Recipe;

  editForm: FormGroup;
  tags: string;
  @ViewChild('recipeThumbnail') thumbnail: ElementRef;
  @ViewChild('recipeImagesNewImage') newImage: ElementRef;
  thumbnailSrc: any;
  imagesSrc: any[] = [];
  isCreateMode = true;

  thumbnailVisited = false;

  langSuffix = '';
  lang = 'english';
  langMap = { english: '', chinese: '_zh' };
  langSuffices = [ '', '_zh' ];

  mouseOverForRemoveButton: {
    type: string,
    partIndex: number,
    stepIndex?: number,
    ingredientIndex?: number
  };

  constructor(private route: ActivatedRoute,
              private router: Router,
              private recipeService: RecipeService,
              private translateService: TranslateService,
              private modalService: ModalService,
              private loadingService: LoadingService,
              private breadcrumbService: BreadcrumbService) {}

  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{
      title: this.translateService.translate('recipes', LANG_EN_NAME),
      title_zh: this.translateService.translate('recipes', LANG_ZH_NAME),
      url: 'recipes'
    }]);

    // init a dummy form before async getting data, it is indeed a form for creation
    this.initDummyForm();

    this._id = this.route.snapshot.params.id;
    if (this._id) {
      this.loadingService.show();
      this.recipeService.getRecipeByIdFromServer(this._id)
        .then((recipe: Recipe) => {
          this.recipe = recipe;
          this.isCreateMode = false;

          this.breadcrumbService.pushBreadcrumb([{
            title: this.recipe.title,
            title_zh: this.recipe.title_zh,
            url: `recipes/edit/${this._id}`
          }]);

          if (this.recipe.thumbnail) {
            this.thumbnailSrc = this.recipe.thumbnail.imageUrl;
            this.thumbnailVisited = true;
          }
          if (this.recipe.images) {
            this.recipe.images.forEach((image: Image) => {
              this.imagesSrc.push(image.imageUrl);
            });
          }

          this.initForm();
        })
        .catch(() => this.modalService.show())
        .then(() => this.loadingService.hide());
    } else {
      this.breadcrumbService.pushBreadcrumb([{
        title: this.translateService.translate('createRecipe', LANG_EN_NAME),
        title_zh: this.translateService.translate('createRecipe', LANG_ZH_NAME),
        url: 'recipes/create'
      }]);
    }
  }

  initDummyForm() {
    this.tags = '';
    this.editForm = new FormGroup({
      title: new FormControl('', Validators.required),
      desc: new FormControl('', Validators.required),
      size: new FormControl('', Validators.required),
      parts: new FormArray([]),
      title_zh: new FormControl('', Validators.required),
      desc_zh: new FormControl('', Validators.required),
      size_zh: new FormControl('', Validators.required),
      parts_zh: new FormArray([])
    });
  }

  initForm() {
    const imagesArray = new FormArray([]);
    this.recipe.images.forEach((image: Image) => {
      imagesArray.push(new FormGroup({
        imageUrl: new FormControl(image.imageUrl, Validators.required)
      }));
    });

    const partsArray = new FormArray([]);
    this.recipe.parts.forEach((part: Part) => {
      const ingredientsArray = new FormArray([]);
      part.ingredients.forEach((ingredient: Ingredient) => {
        ingredientsArray.push(new FormGroup({
          name: new FormControl(ingredient.name, Validators.required),
          quantity: new FormControl(ingredient.quantity, Validators.required),
          unit: new FormControl(ingredient.unit)
        }));
      });

      const stepsArray = new FormArray([]);
      part.steps.forEach((step: Step) => {
        stepsArray.push(new FormGroup({
          line: new FormControl(step.line, Validators.required),
          autoNextStep: new FormControl(step.autoNextStep, Validators.required),
          duration: new FormControl({
            value: step.autoNextStep ? step.duration : 0,
            disabled: !step.autoNextStep
          }, Validators.required)
        }));
      });

      partsArray.push(new FormGroup({
        title: new FormControl(part.title, Validators.required),
        desc: new FormControl(part.desc),
        ingredients: ingredientsArray,
        steps: stepsArray
      }));
    });

    const partsZhArray = new FormArray([]);
    this.recipe.parts_zh.forEach((part: Part) => {
      const ingredientsArray = new FormArray([]);
      part.ingredients.forEach((ingredient: Ingredient) => {
        ingredientsArray.push(new FormGroup({
          name: new FormControl(ingredient.name, Validators.required),
          quantity: new FormControl(ingredient.quantity, Validators.required),
          unit: new FormControl(ingredient.unit)
        }));
      });

      const stepsArray = new FormArray([]);
      part.steps.forEach((step: Step) => {
        stepsArray.push(new FormGroup({
          line: new FormControl(step.line, Validators.required),
          autoNextStep: new FormControl(step.autoNextStep, Validators.required),
          duration: new FormControl({
            value: step.autoNextStep ? step.duration : 0,
            disabled: !step.autoNextStep
          }, Validators.required)
        }));
      });

      partsZhArray.push(new FormGroup({
        title: new FormControl(part.title, Validators.required),
        desc: new FormControl(part.desc),
        ingredients: ingredientsArray,
        steps: stepsArray
      }));
    });

    this.tags = this.recipe.tags.join(', ');
    this.editForm = new FormGroup({
      title: new FormControl(this.recipe.title, Validators.required),
      desc: new FormControl(this.recipe.desc, Validators.required),
      size: new FormControl(this.recipe.size, Validators.required),
      parts: partsArray,
      title_zh: new FormControl(this.recipe.title_zh, Validators.required),
      desc_zh: new FormControl(this.recipe.desc_zh, Validators.required),
      size_zh: new FormControl(this.recipe.size_zh, Validators.required),
      parts_zh: partsZhArray
    });
  }

  isValidForm(): boolean {
    return this.thumbnailSrc !== null && this.editForm.valid;
  }

  getFormArrayControls(parent: any, key: string): AbstractControl[] {
    return (<FormArray> parent.get(key)).controls;
  }

  onChangeDuration(partIndex: number, stepIndex: number) {
    const duration = this.editForm.value['parts' + this.langSuffix][partIndex].steps[stepIndex].duration;

    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            stepCtrl = (<FormArray>partCtrl.get('steps')).at(stepIndex),
            durationCtrl     = stepCtrl.get('duration');
      durationCtrl.patchValue(duration);
    });
  }

  onChangeAutoNextStep(partIndex: number, stepIndex: number) {
    const autoNextStep = this.editForm.value['parts' + this.langSuffix][partIndex].steps[stepIndex].autoNextStep;

    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            stepCtrl = (<FormArray>partCtrl.get('steps')).at(stepIndex),
            autoNextStepCtrl = stepCtrl.get('autoNextStep'),
            durationCtrl     = stepCtrl.get('duration');

      autoNextStepCtrl.patchValue(autoNextStep);

      if (autoNextStep) {
        durationCtrl.enable();
      } else {
        durationCtrl.disable();
        durationCtrl.patchValue(0);
      }
    });

  }

  // --- mouse over styling for remove button ------------------------------

  isMouseOverOn(type: string, partIndex: number, ingredientIndex: number = null, stepIndex: number = null) {
    return this.mouseOverForRemoveButton
      && this.mouseOverForRemoveButton.type === type
      && this.mouseOverForRemoveButton.partIndex === partIndex
      && this.mouseOverForRemoveButton.ingredientIndex === ingredientIndex
      && this.mouseOverForRemoveButton.stepIndex === stepIndex;
  }

  onMouseOverRemoveButton(type: string, partIndex: number, ingredientIndex: number = null, stepIndex: number = null) {
    this.mouseOverForRemoveButton = {
      type: type,
      partIndex: partIndex,
      ingredientIndex: ingredientIndex,
      stepIndex: stepIndex
    };
  }

  onMouseOutRemoveButton() {
    this.mouseOverForRemoveButton = null;
  }

  // -----------------------------------------------------------------------

  // --- dynamic editing form array (steps, ingredients, parts) ------------

  onAddIngredient(partIndex: number) {
    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl         = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            ingredientsArray = <FormArray>partCtrl.get('ingredients');
      ingredientsArray.push(new FormGroup({
        name: new FormControl('', Validators.required),
        quantity: new FormControl('', Validators.required),
        unit: new FormControl('')
      }));
    });
  }

  onRemoveIngredient(partIndex: number, ingredientIndex: number) {
    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl         = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            ingredientsArray = <FormArray>partCtrl.get('ingredients');
      ingredientsArray.removeAt(ingredientIndex);
    });
  }

  onAddStep(partIndex: number) {
    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl   = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            stepsArray = <FormArray>partCtrl.get('steps');
      stepsArray.push(new FormGroup({
        line: new FormControl('', Validators.required),
        autoNextStep: new FormControl(false, Validators.required),
        duration: new FormControl({
          value: 0, disabled: true
        })
      }));
    });
  }

  onRemoveStep(partIndex: number, stepIndex: number) {
    this.langSuffices.forEach((langSuffix: string) => {
      const partCtrl = (<FormArray>this.editForm.get('parts' + langSuffix)).at(partIndex),
            stepsArray = <FormArray>partCtrl.get('steps');
      stepsArray.removeAt(stepIndex);
    });
  }

  onAddPart() {
    this.langSuffices.forEach((langSuffix: string) => {
      (<FormArray> this.editForm.get('parts' + langSuffix)).push(new FormGroup({
        title: new FormControl('', Validators.required),
        desc: new FormControl(''),
        ingredients: new FormArray([]),
        steps: new FormArray([])
      }));
    });

    const partIndex = (<FormArray> this.editForm.get('parts' + this.langSuffices[0])).controls.length - 1;
    this.onAddIngredient(partIndex);
    this.onAddStep(partIndex);
  }

  onRemovePart(partIndex: number) {
    this.langSuffices.forEach((langSuffix: string) => {
      (<FormArray> this.editForm.get('parts' + langSuffix)).removeAt(partIndex);
    });
  }

  // -----------------------------------------------------------------------

  // handlers for complete/ cancel form ------------------------------------

  onCompleteForm() {
    this.loadingService.show();

    const recipe: any = this.editForm.value;
    recipe.parts.forEach((part) => {
      part.steps.forEach((step) => {
        if (step.duration === undefined) {
          step.duration = 0;
        }
      });
    });
    recipe.parts_zh.forEach((part) => {
      part.steps.forEach((step) => {
        if (step.duration === undefined) {
          step.duration = 0;
        }
      });
    });
    recipe.tags = (this.tags || '')
        .split(',')
        .map((tag) => tag.trim())
        .filter((tag) => tag);

    if (this.isCreateMode) { // CREATING A RECIPE
      this.recipeService.createRecipe(recipe, this.thumbnailSrc, this.imagesSrc)
        .then((id: string) => this.router.navigate(['recipes', 'view', id]))
        .catch((error) => this.modalService.show())
        .then(() => this.loadingService.hide());
    } else { // EDITING A RECIPE
      this.recipeService.editRecipe(this._id, recipe, this.thumbnailSrc, this.imagesSrc)
        .then(() => this.router.navigate(['recipes', 'view', this._id]))
        .catch((error) => this.modalService.show())
        .then(() => this.loadingService.hide());
    }
  }

  onCancelForm() {
    this.modalService.show({
      title: this.translateService.translate('confirm'),
      content: this.translateService.translate('recipe-page-sureCancel'),
      showConfirm: true
    });
    this.modalService.onConfirm
      .subscribe(() => {
        if (this.isCreateMode) {
          this.router.navigate(['recipes']);
        } else {
          this.router.navigate(['recipes', 'view', this._id]);
        }
      });
  }

  // -----------------------------------------------------------------------

  // --- handlers for images, thumbnail ------------------------------------

  onChangeThumbnail(chagneEvent: any) {

    if (this.thumbnail.nativeElement.files && this.thumbnail.nativeElement.files[0]) {
      if (this.isExceedFileSizeLimit(this.thumbnail.nativeElement.files[0])) {
        this.modalService.show({
          content: this.translateService.translate('exceedFileSize') + ' (≤' + RecipeEditComponent.MAX_IMAGE_SIZE + ' KB)'
        });
        return ;
      }

      const reader = new FileReader();

      reader.onload = (readerEvent: any) => {
        this.thumbnailSrc = readerEvent.target.result;
        this.thumbnailVisited = true;
      };

      reader.readAsDataURL(this.thumbnail.nativeElement.files[0]);
    }
  }

  onCancelThumbnail() {
    this.thumbnailSrc = null;
  }

  canAddImage() {
    return this.imagesSrc.length + 1 <= RecipeEditComponent.MAX_IMAGES;
  }

  onAddImage(changeEvent: any) {

    if (this.newImage.nativeElement.files && this.newImage.nativeElement.files[0]) {
      if (this.isExceedFileSizeLimit(this.newImage.nativeElement.files[0])) {
        this.modalService.show({
          content: this.translateService.translate('exceedFileSize') + ' (≤' + RecipeEditComponent.MAX_IMAGE_SIZE + ' KB)'
        });
        return ;
      }

      const reader = new FileReader();

      reader.onload = (readerEvent: any) => {
        this.imagesSrc.push(readerEvent.target.result);
      };

      reader.readAsDataURL(this.newImage.nativeElement.files[0]);
    }
  }

  onCancelImages(imageIndex: number) {
    this.imagesSrc.splice(imageIndex, 1);
  }

  // -----------------------------------------------------------------------

  onChangeLanguageTab(lang: string) {
    this.lang = lang;
    this.langSuffix = this.langMap[lang];
  }

  private isExceedFileSizeLimit(file) {
     return file.size / 1024 > RecipeEditComponent.MAX_IMAGE_SIZE;
  }
}
