import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// ************************** ngx by valor **********************************
import { ButtonsModule, TabsModule } from 'ngx-bootstrap';
// **************************************************************************

// ********************** angular-modal-gallery *****************************
import { ModalGalleryModule } from 'angular-modal-gallery';
// **************************************************************************

import { HLTableModule } from '../hl-table/hl-table.module';
import { TranslateModule } from '../services/translate/translate.module';
import { RecipesRoutingModule } from './recipes-routing.module';
import { SharedModule } from '../shared/shared.module';

import { RecipeService } from '../services/recipe.service';
import { RecipesComponent } from './recipes.component';
import { RecipeViewComponent } from './recipe-view/recipe-view.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';

@NgModule({
  declarations: [
    RecipesComponent,
    RecipeViewComponent,
    RecipeEditComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RecipesRoutingModule,
    TranslateModule,
    SharedModule,
    HLTableModule,
    ButtonsModule,
    TabsModule,
    ModalGalleryModule
  ],
  providers: [
    RecipeService
  ]
})
export class RecipesModule {}
