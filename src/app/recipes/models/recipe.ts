import { Part } from './part';
import { Image } from './image';

export enum RecipeStatus {
  ACTIVE = 1,
  SUSPENDED = 2,
  DELETED = 0
}

export interface Recipe {
  title: string;
  desc: string;
  tags?: string[];
  size: string;
  thumbnail: Image;
  images: Image[];
  parts: Part[];

  title_zh: string;
  desc_zh: string;
  size_zh: string;
  parts_zh: Part[];

  _id: string;
  createdAt: string;
  updatedAt: string;
  status: RecipeStatus;
}
