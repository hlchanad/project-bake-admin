export interface Step {
  line: string;
  autoNextStep: boolean;
  duration: number;
}
