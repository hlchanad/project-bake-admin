import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TranslateService } from '../services/translate/translate.service';
import { RecipeService } from '../services/recipe.service';
import { Recipe, RecipeStatus } from './models/recipe';
import { HLTableComponent } from '../hl-table/hl-table.component';
import { ColumnSettings, ColumnType } from '../hl-table/models/column-setting.model';
import { ModalService } from '../services/modal/modal.service';
import { LoadingService } from 'app/services/loading/loading.service';
import { BreadcrumbService } from '../services/breadcrumb.service';
import { LANG_ZH_NAME } from '../services/translate/lang-zh';
import { LANG_EN_NAME } from '../services/translate/lang-en';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss']
})
export class RecipesComponent implements OnInit, OnDestroy {

  allowCreateRecipe = true;
  @ViewChild(HLTableComponent) hlTable: HLTableComponent;

  langChanges: any;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private translateService: TranslateService,
              private recipeService: RecipeService,
              private loadingService: LoadingService,
              private modalService: ModalService,
              private breadcrumbService: BreadcrumbService) {}

  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{
      title: this.translateService.translate('recipes', LANG_EN_NAME),
      title_zh: this.translateService.translate('recipes', LANG_ZH_NAME),
      url: 'recipes'
    }]);
    this.loadingService.show();
    this.initHLTable();
    this.langChanges = this.translateService.languageChanged.subscribe(() => {
      this.initHLTable(true);
    });
  }

  ngOnDestroy() {
    this.langChanges.unsubscribe();
  }

  getColumnSettings(): ColumnSettings {
    // --- Setting for generating hl-table columns ---
    // this column setting's keys should be identical to Recipe object's keys
    return {
      _id: {
        name: '_id',
        title: this.translateService.translate('id'),
        type: ColumnType.ObjectId,
        visible: false,
        filterable: false,
        sortable: true
      },
      title: {
        name: 'title',
        title: this.translateService.translate('title'),
        type: ColumnType.Text,
        visible: true,
        filterable: true,
        sortable: true
      },
      desc: {
        name: 'desc',
        title: this.translateService.translate('desc'),
        type: ColumnType.Text,
        visible: true,
        filterable: true,
        sortable: true,
        charLimit: 100
      },
      tags: {
        name: 'tags',
        title: this.translateService.translate('tags'),
        type: ColumnType.Array,
        visible: false,
        filterable: false,
        sortable: false
      },
      thumbnail: {
        name: 'thumbnail',
        title: this.translateService.translate('thumbnail'),
        type: ColumnType.Photo,
        visible: true,
        filterable: false,
        sortable: false
      },
      size: {
        name: 'size',
        title: this.translateService.translate('size'),
        type: ColumnType.Text,
        visible: false,
        filterable: false,
        sortable: false
      },
      images: {
        name: 'images',
        title: this.translateService.translate('images'),
        type: ColumnType.Array,
        visible: false,
        filterable: false,
        sortable: false
      },
      parts: {
        name: 'parts',
        title: this.translateService.translate('parts'),
        type: ColumnType.Array,
        visible: false,
        filterable: false,
        sortable: false
      },
      title_zh: {
        name: 'title_zh',
        title: this.translateService.translate('title'),
        type: ColumnType.Text,
        visible: false,
        filterable: true,
        sortable: true
      },
      desc_zh: {
        name: 'desc_zh',
        title: this.translateService.translate('desc'),
        type: ColumnType.Text,
        visible: false,
        filterable: true,
        sortable: true,
        charLimit: 100
      },
      size_zh: {
        name: 'size_zh',
        title: this.translateService.translate('size'),
        type: ColumnType.Text,
        visible: false,
        filterable: false,
        sortable: false
      },
      parts_zh: {
        name: 'parts_zh',
        title: this.translateService.translate('parts'),
        type: ColumnType.Array,
        visible: false,
        filterable: false,
        sortable: false
      },
      createdAt: {
        name: 'createdAt',
        title: this.translateService.translate('createdAt'),
        type: ColumnType.Date,
        visible: false,
        filterable: false,
        sortable: false
      },
      updatedAt: {
        name: 'updatedAt',
        title: this.translateService.translate('updatedAt'),
        type: ColumnType.Date,
        visible: false,
        filterable: false,
        sortable: false
      },
      status: {
        name: 'status',
        title: this.translateService.translate('status'),
        type: ColumnType.Dropdown,
        choices: [
          { value: RecipeStatus.ACTIVE, name: this.translateService.translate('active') },
          // { value: RecipeStatus.DELETED, name: this.translateService.translate('deleted') },
          { value: RecipeStatus.SUSPENDED, name: this.translateService.translate('suspended') }
        ],
        visible: true,
        filterable: true,
        sortable: true
      }
    };
  }

  initHLTable(langChanged = false) {
    this.recipeService.getRecipesFromServer()
      .then((recipes: Recipe[]) => {
        const filteredRecipes = recipes.filter((recipe: Recipe) => recipe.status !== RecipeStatus.DELETED);
        this.hlTable.initData(this.getColumnSettings(), filteredRecipes, langChanged);
      })
      .catch((error) => this.modalService.show())
      .then(() => this.loadingService.hide());
  }

  onCellClick(event: { row: any, column: string }) {
    this.router.navigate(['view', event.row._id], { relativeTo: this.route });
  }

  onCreate() {
    this.router.navigate(['create'], { relativeTo: this.route });
  }
}
