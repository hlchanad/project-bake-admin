import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Recipe, RecipeStatus } from '../models/recipe';
import { RecipeService } from '../../services/recipe.service';
import { ModalService } from '../../services/modal/modal.service';
import { TranslateService } from '../../services/translate/translate.service';
import { LoadingService } from '../../services/loading/loading.service';
import { Image } from '../models/image';
import { BreadcrumbService } from '../../services/breadcrumb.service';
import { LANG_EN_NAME } from '../../services/translate/lang-en';
import { LANG_ZH_NAME } from '../../services/translate/lang-zh';

@Component({
  selector: 'app-recipe-view',
  templateUrl: './recipe-view.component.html',
  styleUrls: ['./recipe-view.component.scss']
})
export class RecipeViewComponent implements OnInit {

  _id: string;
  recipe: Recipe;

  showImageModal = false;
  imagePointer = 0;
  images: { thumb: string, img: string, description: string }[] = [];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private recipeService: RecipeService,
              private translateService: TranslateService,
              private loadingService: LoadingService,
              private modalService: ModalService,
              private breadcrumbService: BreadcrumbService) {}

  ngOnInit() {
    this.breadcrumbService.setBreadcrumb([{
      title: this.translateService.translate('recipes', LANG_EN_NAME),
      title_zh: this.translateService.translate('recipes', LANG_ZH_NAME),
      url: 'recipes'
    }]);

    this.loadingService.show();
    this._id = this.route.snapshot.params.id;
    this.recipeService.getRecipeByIdFromServer(this._id)
      .then((recipe: Recipe) => {
        if (recipe.status === RecipeStatus.DELETED) {
          this.router.navigate(['recipes']);
          this.modalService.show({
            content: this.translateService.translate('noSuchRecipe')
          });
        } else {
          this.recipe = recipe;

          this.breadcrumbService.pushBreadcrumb([{
            title: this.recipe.title,
            title_zh: this.recipe.title_zh,
            url: `recipes/view/${this._id}`
          }]);

          this.recipe.images.forEach((image: Image) => {
            this.images.push({
              thumb: image.imageUrl,
              img: image.imageUrl,
              description: ''
            });
          });
        }
      })
      .catch((error) => {
        if (error === 'no_such_recipe') {
          this.modalService.show({ content: this.translateService.translate('noSuchRecipe') });
        } else {
          this.modalService.show();
        }
      })
      .then(() => this.loadingService.hide());
  }

  isAbleToEdit() {
    return this.isAbleToDelete();
  }

  isAbleToActivate() {
    return this.recipe && this.recipe.status === RecipeStatus.SUSPENDED;
  }

  isAbleToSuspend() {
    return this.recipe && this.recipe.status === RecipeStatus.ACTIVE;
  }

  isAbleToDelete() {
    return this.recipe && this.recipe.status !== RecipeStatus.DELETED;
  }

  onEdit() {
    this.router.navigate(['recipes', 'edit', this._id]);
  }

  onSuspend() {
    this.modalService.show({
      title: this.translateService.translate('confirm'),
      content: this.translateService.translate('recipe-page-sureSuspend'),
      showConfirm: true
    });
    const subscription = this.modalService.onConfirm
      .subscribe(() => {
        this.loadingService.show();
        subscription.unsubscribe();
        this.recipeService.suspendRecipe(this._id)
          .then(() => {
            this.recipeService.getRecipeByIdFromServer(this._id)
              .then((recipe: Recipe) => this.recipe = recipe)
              .catch((error) => this.modalService.show());
          })
          .catch((error) => this.modalService.show())
          .then(() => this.loadingService.hide());
      });
  }

  onActivate() {
    this.loadingService.show();
    this.recipeService.activateRecipe(this._id)
      .then(() => {
        this.recipeService.getRecipeByIdFromServer(this._id)
          .then((recipe: Recipe) => this.recipe = recipe)
          .catch((error) => this.modalService.show());
      })
      .catch((error) => this.modalService.show())
      .then(() => this.loadingService.hide());
  }

  onDelete() {
    this.modalService.show({
      title: this.translateService.translate('confirm'),
      content: this.translateService.translate('recipe-page-sureDelete'),
      showConfirm: true
    });
    const subscription = this.modalService.onConfirm
      .subscribe(() => {
        this.loadingService.show();
        subscription.unsubscribe();
        this.recipeService.deleteRecipe(this._id)
          .then(() => this.router.navigate(['recipes']))
          .catch(() => this.modalService.show())
          .then(() => this.loadingService.hide());
      });
  }

  onClickImage(imageIndex: number) {
    this.imagePointer = imageIndex;
    this.showImageModal = true;
  }

  onCloseImageModal(event: any) {
    this.showImageModal = false;
    this.imagePointer = 0;
  }
}
