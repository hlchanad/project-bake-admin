import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecipesComponent } from './recipes.component';
import { RecipeViewComponent } from './recipe-view/recipe-view.component';
import { RecipeEditComponent } from './recipe-edit/recipe-edit.component';

const routes: Routes = [
  { path: '', component: RecipesComponent },
  { path: 'view/:id', component: RecipeViewComponent },
  { path: 'edit/:id', component: RecipeEditComponent },
  { path: 'create', component: RecipeEditComponent }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class RecipesRoutingModule {}
